﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebApplication.Areas.UK.Controllers;
using WebApplication.Areas.UK.Models;

namespace WebApplication.Areas.UK.Controllers
{
    public class UKController : Controller
    {
        // GET: UK/UK/index

        List<UKModel> modelList = new List<UKModel>();
        public ActionResult Index()
        {
            UKModel model = new UKModel();
            string file = Server.MapPath("~/jsondata/AddressListUK.json");
            string file1 = Server.MapPath("~/jsondata/PostCodeListUK.json");

            string Json = System.IO.File.ReadAllText(file);
            string Json1 = System.IO.File.ReadAllText(file1);

            JavaScriptSerializer ser = new JavaScriptSerializer();
            model.AddressList = ser.Deserialize<List<AddressList>>(Json);

            model.PostCode_List = ser.Deserialize<List<PostCodeList>>(Json1);

            return View(model);
        }
        [HttpPost]
        public ActionResult Index(UKModel model)
        {
            modelList.Add(model);
            return RedirectToAction("RiskView", "UK");
        }

        [HttpGet]
        public ActionResult RiskView()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RiskView(UKModel model)
        {
            modelList.Add(model);
            return RedirectToAction("Quote", "UK", model);
        }
        [HttpGet]
        public ActionResult Quote(UKModel model)
        {
            int bikePrice = 50000;
            int fixedBikePrice = 50000;
            int depreciationValue = 0;
            int fixedPremium = 6000;
            int dynamicPremium = 0;


            int diffYear = 2019 - Convert.ToInt16(model.Years);
            if (diffYear == 0)
            {
                var totalCoverage = fixedBikePrice - fixedBikePrice * depreciationValue / 100;
                model.Tcov = Convert.ToString(totalCoverage);

                model.Mins = Convert.ToString(bikePrice / 6);
                var x = ((fixedPremium + dynamicPremium) / 5);
                model.Tpre = x.ToString();
            }
            if (diffYear == 1)
            {
                depreciationValue = 15;
                bikePrice = 50000;
                dynamicPremium = 6000;

                var totalCoverage = fixedBikePrice - fixedBikePrice * depreciationValue / 100;
                model.Tcov = Convert.ToString(totalCoverage);

                model.Mins = Convert.ToString(bikePrice / 6);
                var x = ((fixedPremium + dynamicPremium) / 5);
                model.Tpre = x.ToString();

            }
            else if (diffYear == 2)
            {
                depreciationValue = 20;
                bikePrice = 52000;
                dynamicPremium = (fixedPremium * 10) / 100;

                var totalCoverage = fixedBikePrice - fixedBikePrice * depreciationValue / 100;
                model.Tcov = Convert.ToString(totalCoverage);

                model.Mins = Convert.ToString(bikePrice / (diffYear * 12));
                var x = ((fixedPremium + dynamicPremium) / 5);
                model.Tpre = x.ToString();


            }
            else if (diffYear == 3)
            {
                depreciationValue = 30;
                bikePrice = 53000;
                dynamicPremium = (fixedPremium * 20) / 100;

                var totalCoverage = fixedBikePrice - fixedBikePrice * depreciationValue / 100;
                model.Tcov = Convert.ToString(totalCoverage);

                model.Mins = Convert.ToString(bikePrice / (diffYear * 12));
                var x = ((fixedPremium + dynamicPremium) / 5);
                model.Tpre = x.ToString();
            }
            else if (diffYear == 4)
            {
                depreciationValue = 40;
                bikePrice = 54000;
                dynamicPremium = (fixedPremium * 30) / 100;

                var totalCoverage = fixedBikePrice - fixedBikePrice * depreciationValue / 100;
                model.Tcov = Convert.ToString(totalCoverage);

                model.Mins = Convert.ToString(bikePrice / (diffYear * 12));
                var x = ((fixedPremium + dynamicPremium) / 5);
                model.Tpre = x.ToString();
            }
            else if (diffYear == 5)
            {
                depreciationValue = 50;
                bikePrice = 55000;
                dynamicPremium = (fixedPremium * 40) / 100;

                var totalCoverage = fixedBikePrice - fixedBikePrice * depreciationValue / 100;
                model.Tcov = Convert.ToString(totalCoverage);

                model.Mins = Convert.ToString(bikePrice / (diffYear * 12));
                var x = ((fixedPremium + dynamicPremium) / 5);
                model.Tpre = x.ToString();
            }
            else
            {
                ViewBag.Message = "Sorry , we don't provide insurance for the same";
            }

            modelList.Add(model);

            return View(model);
        }

        [HttpPost]
        public ActionResult Quote()
        {
            return View();
        }
        public List<AddressList> Address_For_PostCode(String PostCode)
        {
            List<AddressList> addressList = new List<AddressList>();
            List<PostCodeList> postCodeList = new List<PostCodeList>();

            string file = Server.MapPath("~/jsondata/AddressListUK.json");
            string file1 = Server.MapPath("~/jsondata/PostCodeListUK.json");

            string Json = System.IO.File.ReadAllText(file);
            string Json1 = System.IO.File.ReadAllText(file1);

            JavaScriptSerializer ser = new JavaScriptSerializer();
            addressList = ser.Deserialize<List<AddressList>>(Json);
            postCodeList = ser.Deserialize<List<PostCodeList>>(Json1);

            int a = Convert.ToInt32(PostCode);

            var post_code_new = postCodeList.Where(X => X.Id == a).Select(X => X.PostCode).FirstOrDefault();

            addressList = addressList.Where(X => X.PostCode == post_code_new).Select(x => new AddressList
            {
                PostCode = x.PostCode,
                Address = x.Address
            }).Distinct().ToList();

            return (addressList);
        }

        public JsonResult GetAddress_For_PostCode(string PostCode)
        {
            return Json(Address_For_PostCode(PostCode));
        }

    }
}