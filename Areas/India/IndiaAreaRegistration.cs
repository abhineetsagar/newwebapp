﻿using System.Web.Mvc;

namespace WebApplication.Areas.India
{
    public class IndiaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "India";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
           
            context.MapRoute(
                "India_default",
                "India/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
             "IndiaRoute",
             "India/IndiaView",
             new { action = "Index", id = UrlParameter.Optional }
         );


        }
    }
}