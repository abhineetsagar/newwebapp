﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Areas.India.Models
{
    public class AddressList
    {
        public string PostCode { get; set; }
        public string Address { get; set; }
    }
}