﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebApplication.Areas.India.Models
{
    public class IndiaModel
    {
        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Invalid")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "   Date of Birth")]
        public DateTime Dob { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        [Display(Name = "Mobile Number")]
        public string Phone { get; set; }


        
        [Display(Name = "Post Code")]
        public string Post_Code { get; set; }

        public List <PostCodeList> PostCode_List { get; set; }
        
       
        public string _Address { get; set; }

       
        [Display(Name = "Address")]
        public List<AddressList> AddressList { get; set; }

        //-------------------------------------------------------------------------------------
        [Required]
        [Display(Name = " Vehicle Number")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "Invalid vehicle number :Enter correct 8 digit vehicle number")]
        public string VehicleNumber { get; set; }


        [Required]
        [Display(Name = " Engine Number")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "Invalid Engine Number : Ensure you have entered 13 digit correct engine number")]
        public string EngineNumber { get; set; }

        [Required]
        [Display(Name = "No. of KM's")]
        [StringLength(8, MinimumLength = 2, ErrorMessage = "We don't allow single digit distance covered")]
        // [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid format")]
        public string Kms { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "Invalid year entered")]

        [Display(Name = "Year of purchase")]
        // [DisplayFormat()
        [RegularExpression(@"(2014|2015|2016|2017|2018|2019)", ErrorMessage = "We are giving insurance only for purchased vehicles from 2014 till current year")]
        public string Years { get; set; }


        //-------------------------------------------------------------------------------------
        [Display(Name = "Monthly Installment Payable on purchasing Vehicle(In ₹) ")]
        //[DisplayFormat(DataFormatString = "{0:n2}")] 
        public string Mins { get; set; }

        [Display(Name = "Total Premium given to insurer based on the year you purchased the vehicle(In ₹)")]

        [DisplayFormat(DataFormatString = "{0:n2}")]
        //[Display(Name = "Total Premium")]
        public string Tpre { get; set; }

        [Display(Name = "Total Coverage after the depreciated value for your vehicle(In ₹)")]
        //[DisplayFormat(DataFormatString = "{0:n2}")]
        public string Tcov { get; set; }

    }

}