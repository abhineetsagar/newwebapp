﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Areas.India.Models
{
    public class PostCodeList
    {
        public int Id { get; set; }

        public string PostCode { get; set; }
    }
}