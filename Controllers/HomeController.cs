﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        //Home Index mthod in Home controller
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Just enter your vehicle information to get a Quote about your premiums";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact me on the mentioned Email-id";

            return View();
        }
    }
}